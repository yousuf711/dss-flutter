import 'package:dss_flutter/app_route.dart';
import 'package:dss_flutter/features.dss/api_service/api_status.dart';
import 'package:dss_flutter/features.dss/presentation/provider/auth_provider.dart';
import 'package:dss_flutter/features.dss/presentation/widgets/TextCommon.dart';
import 'package:dss_flutter/features.dss/presentation/widgets/TextFieldCommon.dart';
import 'package:dss_flutter/features.dss/presentation/widgets/button_common.dart';
import 'package:dss_flutter/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'injection_container.dart' as di;


//flutter run --no-sound-null-safety
void main() async {
  Provider.debugCheckInvalidValueType = null;
  _setupLogging();
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: AppRoute().myRoute,
      // home: Provider(
      //   create: (_) => sl<AuthProvider>(),
      //   child: MyHomePage(),
      // ),
    );
  }
}


void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print('${rec.level.name}: ${rec.time}: ${rec.message}');
  });
}
