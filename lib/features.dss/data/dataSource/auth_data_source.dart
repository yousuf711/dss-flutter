import 'dart:convert';

import 'package:dss_flutter/core/error/exceptions.dart';
import 'package:dss_flutter/features.dss/api_service/login_api_service.dart';
import 'package:dss_flutter/features.dss/data/model/login_info_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:dss_flutter/core/error/api_error.dart';

abstract class LoginDataSource {
  /// Throws [CacheException] if no cached data is present.
  Future<dynamic> userLogin(Map<String, dynamic> map);
}

class LoginDataSourceImpl implements LoginDataSource {
  LoginApiService? loginApiService;

  LoginDataSourceImpl({@required this.loginApiService});

  @override
  Future<dynamic> userLogin(Map<String, dynamic> map) async {

    try{
      var res = await loginApiService?.login(map);

      if (res == null) {
        throw CacheException();
      } else if (res.isSuccessful) {
            return LoginInfoModel.fromJson(res.body['data']);
      } else {
        return ApiError.fromJson(json.decode(res.error.toString()));
      }
    }catch(e){
      print('...................res res res res       ${e}');
      throw CacheException();
    }
  }
}
