// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginInfoModel _$LoginInfoModelFromJson(Map<String, dynamic> json) {
  return LoginInfoModel(
    json['id'] as int,
    json['employee_id'] as int,
    json['status'] as int,
    json['phone_number'] as String,
    json['username'] as String,
    json['email'] as String,
    json['email_verified_at'] as String,
    json['token'] as String,
    json['password'] as String,
  );
}

Map<String, dynamic> _$LoginInfoModelToJson(LoginInfoModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'employee_id': instance.employee_id,
      'status': instance.status,
      'phone_number': instance.phone_number,
      'username': instance.username,
      'email': instance.email,
      'email_verified_at': instance.email_verified_at,
      'token': instance.token,
      'password': instance.password,
    };
