import 'dart:collection';

import 'package:json_annotation/json_annotation.dart';

part 'login_info_model.g.dart';

@JsonSerializable()
class LoginInfoModel{

  int? id,employee_id,status;
   String?  phone_number,username,email,email_verified_at,token,password;

  LoginInfoModel(
      this.id,
      this.employee_id,
      this.status,
      this.phone_number,
      this.username,
      this.email,
      this.email_verified_at,
      this.token,
      this.password);
  factory  LoginInfoModel.fromJson( Map<String,dynamic> json) => _$LoginInfoModelFromJson(json);
  Map<String,dynamic> toJson() => _$LoginInfoModelToJson(this);

}