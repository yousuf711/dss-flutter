import 'package:dss_flutter/features.dss/api_service/api_status.dart';
import 'package:dss_flutter/features.dss/presentation/provider/auth_provider.dart';
import 'package:dss_flutter/features.dss/presentation/widgets/TextCommon.dart';
import 'package:dss_flutter/features.dss/presentation/widgets/TextFieldCommon.dart';
import 'package:dss_flutter/features.dss/presentation/widgets/button_common.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:provider/provider.dart';


class LoginPage extends StatelessWidget {
  static const pageId='loginPage';
  //const MyHomePage({ Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    AuthProvider auth =Provider.of<AuthProvider>(context, listen: true);
    // _userLogin();
    // Provider.of<AuthProvider>(context, listen: true).login('admin@domain.com','12345678');
    return Scaffold(
      body:StreamBuilder(
        stream: auth.isLoadingStream.stream,
        builder: (ctx,snap){
          return  LoadingOverlay(child: Container(
            margin: EdgeInsets.all(25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextCommon(title: 'User Id',fontSize: 20.0,fontWeight: FontWeight.bold,),
                SizedBox(
                  height: 15,
                ),
                StreamBuilder(
                    stream: auth.userIdErrorStream.stream,
                    builder: (_,snapshort){
                      return   TextFieldCommon(controller: auth.userIdController,
                          hint: "User Id",
                          icon: Icon(Icons.message),
                          errorText: snapshort.data);
                    }),
                SizedBox(
                  height: 15,
                ),
                TextCommon(title: 'Password',fontSize: 20.0,fontWeight: FontWeight.bold,),
                SizedBox(
                  height: 15,
                ),
                StreamBuilder(
                    stream: auth.passwordErrorStream.stream,
                    builder: (_,snapshort){
                      return   TextFieldCommon(controller: auth.passwordController,
                          hint: "password",
                          icon: Icon(Icons.message),
                          isSecure: true,
                          errorText: snapshort.data);
                    }),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: TextButtonCommon(
                    text: 'Login',
                    callBack: ()async {
                      //  await auth.login('admin@domain.com', '12345678');
                      await auth.login(auth.userIdController.text.trim().toString(), auth.passwordController.text.trim().toString());

                      switch (auth
                          .apiStatus) {
                        case ApiStatus.LOADING:
                          print('..............LOADING LOADING..................');
                          break;
                        case ApiStatus.SUCCESS:
                          print(
                              '..............SUCCESS SUCCESS..................${Provider.of<AuthProvider>(context, listen: false).loginInfoModel?.phone_number}');
                          break;
                        case ApiStatus.APIERROR:
                          print(
                              '..............APIERROR APIERROR..................${Provider.of<AuthProvider>(context, listen: false).apiError?.message}');
                          break;
                      }
                    },
                  ),
                )
              ],
            ),
          ),isLoading:  snap.data as bool ?? false);
        },
      ),
    );
  }
}