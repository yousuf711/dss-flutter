import 'dart:async';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dss_flutter/core/error/ErrorUtil.dart';
import 'package:dss_flutter/core/network/network_info.dart';
import 'package:dss_flutter/features.dss/api_service/api_status.dart';
import 'package:dss_flutter/features.dss/api_service/login_api_service.dart';
import 'package:dss_flutter/features.dss/data/dataSource/auth_data_source.dart';
import 'package:dss_flutter/features.dss/data/model/login_info_model.dart';
import 'package:dss_flutter/features.dss/domain/repositoris/auth_repo.dart';
import 'package:dss_flutter/features.dss/domain/usecase/user_login.dart';
import 'package:flutter/cupertino.dart';
import 'package:dss_flutter/core/error/api_error.dart';
import 'package:flutter/foundation.dart';

class AuthProvider with ChangeNotifier, DiagnosticableTreeMixin {

  UserLogin userLogin ;
  ApiStatus? apiStatus = ApiStatus.INITIALIZE;
  ApiError? apiError;
  LoginInfoModel? loginInfoModel;
  StreamController<bool> isLoadingStream = StreamController<bool>();
  StreamController<String?> userIdErrorStream = StreamController<String>();
  StreamController<String?> passwordErrorStream = StreamController<String>();

  TextEditingController userIdController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  AuthProvider({required this.userLogin}) : super(){
    isLoadingStream.add(false);
    clearAllStream();
  }

  dynamic login(String email, String password) async {
    isLoadingStream.add(true);
      apiStatus = ApiStatus.LOADING;
     notifyListeners();
    clearAllStream();
    Map<String, dynamic> map = {'userid': email, 'password': password};
    try {
      print('...................$map.......................................');
      var result = await userLogin(map);
      isLoadingStream.add(false);
      result.fold(
          (failure) => {
                apiStatus = ApiStatus.FAILED,
                print('..............server error..................${failure}'),
                notifyListeners()
              },
          (response) => {
                if (response.runtimeType == ApiError)
                  {
                    apiError = response as ApiError,
                    apiStatus = ApiStatus.APIERROR,
                    _showError(),
                    notifyListeners(),
                    print(
                        '..............ApiError ggg b..................${(response as ApiError).message}')
                  }
                else
                  {
                    loginInfoModel = response as LoginInfoModel,
                    apiStatus = ApiStatus.SUCCESS,
                    notifyListeners(),
                    //    apiStatus = ApiStatus.LOADING,
                    //  notifyListeners(),
                    print(
                        '..............LoginInfoModel ..................${(response as LoginInfoModel).email}')
                  },
              });
    } catch (error) {
      apiStatus = ApiStatus.FAILED;
      print('..............exception mmmmmmmmmmmm..................$error');
      notifyListeners();
    }

    // /// Makes `Counter` readable inside the devtools by listing all of its properties
    // @override
    // void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    //   super.debugFillProperties(properties);
    //  // properties.add(IntProperty('count', count));
    // }
  }

  void _showError() {
    //userIdErrorStream.add('kkkkkkkkkk');
    try {
      if (apiError?.errors != null) {
        int len = apiError?.errors?.length as int;
        for (int i = 0; i < len; i++) {
          var error = apiError?.errors![i]?.field;
          var message = apiError?.errors![i]?.message;

          if (error == null) {
            userIdErrorStream.add(ErrorUtil().mainError(message));
          }
          switch (error) {
            case 'email':
              {
                userIdErrorStream.add(ErrorUtil().mainError(message));
                break;
              }
            case 'password':
              {
                passwordErrorStream.add(ErrorUtil().mainError(message));
                break;
              }
          }
        }
      }
    } catch (e) {}
  }

  void clearAllStream() {
    userIdErrorStream.add(null);
    passwordErrorStream.add(null);
  }
}
