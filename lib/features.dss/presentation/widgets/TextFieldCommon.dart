import 'package:flutter/material.dart';

class TextFieldCommon extends StatelessWidget {
  var hint, errorText;
  TextEditingController? controller;
  Icon? icon;
  TextInputType? keyboardType;
  bool? isSecure;

  TextFieldCommon(
      {required this.controller, required this.hint, this.errorText, this.icon,this.keyboardType,this.isSecure});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TextField(
            controller: controller,
            obscureText: isSecure??false,
            keyboardType: keyboardType ?? TextInputType.text,
            textAlign: TextAlign.left,
            decoration: InputDecoration(
              // icon: Icon(Icons.send), //  outside box icon....................................
              prefixIcon: icon,
              hintText: hint,
              helperText: null,
              errorText: errorText,
              counterText: null,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              hintStyle: TextStyle(color: Colors.grey),
            ),
          ),
        ],
      ),
    );
  }
}
