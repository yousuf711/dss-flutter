import 'package:chopper/chopper.dart';
import 'package:dss_flutter/core/network/httpinterceptor.dart';
import 'package:dss_flutter/core/util/static_key.dart';
import 'package:dss_flutter/core/error/api_error.dart';


part 'login_api_service.chopper.dart';

@ChopperApi()
abstract class LoginApiService extends ChopperService{


  @Post(path: '/api/auth/login')
  Future<Response> login(
      @Body() Map<String,dynamic> body,
      );

  static LoginApiService create() {
    final client = ChopperClient(
      baseUrl: StaticKey.BASE_URL,
      services: [
        _$LoginApiService(),
      ],
      converter: JsonConverter(),
      interceptors: [HttpLoggingInterceptor(),HeaderInterceptor()],
    );
    return _$LoginApiService(client);
  }

}