import 'package:dartz/dartz.dart';
import 'package:dss_flutter/core/usecase/common_usecase.dart';
import 'package:dss_flutter/core/error/failures.dart';
import 'package:dss_flutter/features.dss/domain/repositoris/auth_repo.dart';

class UserLogin implements  CommonUseCase<dynamic, Map<String,dynamic>>{
  final AuthRepository repository;

  UserLogin(this.repository);

  @override
  Future<Either<Failure, dynamic>> call(Map<String,dynamic> params) async {
    return await repository.login(params);
  }
}


