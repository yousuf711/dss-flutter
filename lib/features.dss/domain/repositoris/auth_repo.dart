import 'package:dartz/dartz.dart';
import 'package:dss_flutter/core/error/failures.dart';
import 'package:dss_flutter/core/network/network_info.dart';
import 'package:dss_flutter/features.dss/data/dataSource/auth_data_source.dart';

abstract class AuthRepository {
  Future<Either<Failure, dynamic>> login(Map<String, dynamic> map);
}

class AuthRepositoryImpl implements AuthRepository {
  final LoginDataSource loginDataSource;
  final NetworkInfo networkInfo;

  AuthRepositoryImpl(this.loginDataSource, this.networkInfo);

  @override
  Future<Either<Failure, dynamic>> login(Map<String, dynamic> map) {
   return _userLogin(map);
  }

  Future<Either<Failure, dynamic>> _userLogin(Map<String, dynamic> map) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteTrivia = await loginDataSource.userLogin(map);
        return Right(remoteTrivia);
      } catch(e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(InternetFailure());
    }
  }
}
