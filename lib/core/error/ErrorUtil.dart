

class  ErrorUtil{

  String mainError(List<dynamic?>? message) {
    var error = "";
    int len = message?.length as int;
    if(message!=null)
    for (int i = 0; i < len; i++) error += message![i]! + " ";
    return error;
  }
}