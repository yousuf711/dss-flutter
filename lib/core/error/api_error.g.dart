// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiError _$ApiErrorFromJson(Map<String, dynamic> json) {
  return ApiError(
    json['code'] as int,
    json['status'] as String,
    json['message'] as String,
    (json['errors'] as List)
        ?.map((e) =>
            e == null ? null : Errors.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ApiErrorToJson(ApiError instance) => <String, dynamic>{
      'code': instance.code,
      'status': instance.status,
      'message': instance.message,
      'errors': instance.errors,
    };

Errors _$ErrorsFromJson(Map<String, dynamic> json) {
  return Errors(
    json['field'] as String,
    json['value'] as String,
    json['message'] as List,
  );
}

Map<String, dynamic> _$ErrorsToJson(Errors instance) => <String, dynamic>{
      'field': instance.field,
      'value': instance.value,
      'message': instance.message,
    };
