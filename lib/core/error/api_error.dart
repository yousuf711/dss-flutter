import 'package:json_annotation/json_annotation.dart';

part 'api_error.g.dart';

@JsonSerializable()
class ApiError {
  int? code;
  String? status, message;
  List<Errors?>? errors;

  ApiError(this.code, this.status, this.message, this.errors);

  factory ApiError.fromJson(Map<String, dynamic> json) =>
      _$ApiErrorFromJson(json);

  Map<String, dynamic> toJson() => _$ApiErrorToJson(this);


}

@JsonSerializable()
class Errors {
  String? field, value;
  List<dynamic?>? message;

  Errors(this.field, this.value, this.message);

  factory Errors.fromJson(Map<String, dynamic> json) => _$ErrorsFromJson(json);

  Map<String, dynamic> toJson() => _$ErrorsToJson(this);
}
