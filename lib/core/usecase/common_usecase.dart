import 'package:dartz/dartz.dart';
import 'package:dss_flutter/core/error/failures.dart';
import 'package:equatable/equatable.dart';


abstract class CommonUseCase<Type, Params> {
  Future<Either<Failure, Type>> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object> get props => [];
}