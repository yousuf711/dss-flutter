import 'package:dss_flutter/features.dss/presentation/provider/auth_provider.dart';
import 'package:dss_flutter/features.dss/presentation/view/login_page.dart';
import 'package:dss_flutter/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';



class AppRoute {

  Route myRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (_) => Provider(
              create: (_) => sl<AuthProvider>(),
              child: LoginPage(),
            ),);
      // case '/weatherDetail':
      //   Weather weather = routeSettings.arguments as Weather;
      //   return MaterialPageRoute(
      //       builder: (_) => BlocProvider.value(
      //           value: weatherBloc,
      //           child: WeatherDetailPage(masterWeather: weather)));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${routeSettings.name}')),
            ));
    }
  }

  onDispose() {
  }
}
