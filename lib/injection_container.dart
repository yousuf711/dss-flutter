import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'core/network/network_info.dart';
import 'features.dss/api_service/login_api_service.dart';
import 'features.dss/data/dataSource/auth_data_source.dart';
import 'features.dss/domain/repositoris/auth_repo.dart';
import 'features.dss/domain/usecase/user_login.dart';
import 'features.dss/presentation/provider/auth_provider.dart';


GetIt sl = GetIt.instance;

Future<void> init() async {
  //! Features - Login
  // Provider
  sl.registerFactory(
        () => AuthProvider(
      userLogin: sl()
    ),
  );


  // Use cases
  sl.registerLazySingleton<UserLogin>(() => UserLogin(sl()));


  // Repository
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(sl(), sl()));

  // Data sources
  sl.registerLazySingleton<LoginDataSource>(() => LoginDataSourceImpl(loginApiService:sl()));

  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  //api service
  sl.registerLazySingleton<LoginApiService>(() => LoginApiService.create());
  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
}